### Description
Script de mise à jour du système pour les distributions basées sur Debian. Testé Ubuntu.

Teste les paquets à mettre à jour, propose la mise à jour, purge le gestionnaire de mise à jour APT et aptitude, tient un journal des mises à jours.

#### Version 1.0
* Version graphique : Zenity.
* Update - upgrade - purge.

#### Pré-requis
Nécessite que l'utilisateur n'est pas besoin de mot de passe pour exécuter en sudo les commandes aptitude, apt-get et dpkg. 
Marche à suivre :
* `sudo nano /etc/sudoers`
* ajouter `nom_utilisateur ALL=(root) NOPASSWD:/usr/bin/aptitude,/usr/bin/apt-get,/usr/bin/dpkg`